#include <stdio.h>
#include <math.h>

float sigmoid(float x)
{
    float sigmoid = 1 / (1+exp(-x));
    return sigmoid;
}

int main()
{
    float num = -5;
    do {
        printf("%0.3f %0.3f\n", num, sigmoid(num));
        num=num+1;

    } while( num <= 5 );
}