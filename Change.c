#include <stdio.h>
#include <math.h>

int getAmount(int x)
{
    printf("Enter a dollar amount:");
    scanf("%d", &x);
    while(x < 0)
    {
        printf("The number needs to be a positive integer:");
       scanf("%d", &x);
    }
    
    return x;
}

int bills(int y, int z)
{
    int billSum, i;
    billSum=y;

    do
    {
        if (billSum>=100)
        {
            z=z+billSum/100;
            i=billSum%100;
            billSum=i;   
        }

        if (billSum>=20&&billSum<100)
        {
            z=z+billSum/20;
            i=billSum%20;
            billSum=i;
        }
    
        if (billSum>=10&&billSum<20)
        {
            z=z+billSum/10;
            i=billSum%10;
            billSum=i;
        }

        if (billSum>=5&&billSum<10)
        {
            z=z+billSum/5;
            i=billSum%5;
            billSum=i;
        }

        if (billSum>=1&&billSum<5)
        {
            z=z+billSum/1;
            i=billSum%1;
            billSum=i;
        }

    } 
    while (billSum!=0);

    return z;
}

int main()
{
    int amntGvn, numBills;
    printf("You get %d bills back.\n", bills(getAmount(amntGvn), numBills));
}