#include <stdio.h>
#include <math.h>

int getStart(int x)
{
    printf("Enter a positive integer:");
    scanf("%d", &x);
    while(x < 0)
    {
        printf("The number needs to be a positive integer.\n");
        scanf("%d", &x);
    }
    
    return x;
}

int nextCollatz(int y)
{
    if(y%2==0)
    {
        y=y/2;
        return y;
    }

    else if(y%2==1)
    {
        y=3*y+1;
        return y;
    }

    return y;
}

int main()
{
    int usrInput, stordNum, numCount;
    stordNum = getStart(usrInput);
    numCount = 0;
    do
    {
        if (stordNum!=1)
        {
            printf("%d, ", nextCollatz(stordNum));
            stordNum = nextCollatz(stordNum);
            numCount+=1;
        }

        else
        {
            printf("1 \n");
            break;
        }
    }
    while(stordNum!=0);

    printf("Length: %d\n", numCount);

    return 0;
}